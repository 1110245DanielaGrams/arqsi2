<?php
require_once('nusoap.php');
// Create the server instance
$server = new soap_server;
// Initialize WSDL support
$server->configureWSDL( 'servicename', 'urn:servicename', '', 'document');
$in = array('x'=>'xsd:int',
'y'=>'xsd:int');
$out = array('Pass' => 'xsd:int');
// registar serviço
$server->register(
"Add", //methodname
$in,// parâmetros de entrada
$out,// parâmetro de saída
'urn:servicename', // namespace
$server->wsdl->endpoint .'#'. "Add", // soapaction
'document', // style
'literal', // use
'N/A' // documentation
);
// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ?
$HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
// Define the method as a PHP function
function add($x,$y) {
$result=$x+$y;
return array('Pass'=>$result);
}
?>