﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IDEIMusic.DAL;
using System.Web.Mvc;
using System.Web.Helpers;

namespace IDEIMusic
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IIdeiService" in both code and config file together.
    [ServiceContract]
    public interface IIdeiService
    {
        [OperationContract]
        string getCatalog(string Api);

        [OperationContract]
        string getApiKey(string store);

        [OperationContract]
        string sendOrder(string order);

        [OperationContract]
        void getEncomenda(string order, string apiKey);

        [OperationContract]
        void DoWork();
    }
}
