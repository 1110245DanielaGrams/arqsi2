﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IDEIMusic.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace IDEIMusic.DAL
{
    public class MusicContext: DbContext
    { 
        public MusicContext() : base("MusicContext"){ //nome da connectionString (adicionado ao Web.config)

    }

        public DbSet<Album> Albuns { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>(); //prevents table name pluralizing
        }

        //public System.Data.Entity.DbSet<IDEIMusic.Models.User> Users { get; set; }

    }
}