﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using IDEIMusic.Models;

namespace IDEIMusic.DAL
{
    public class MusicInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<MusicContext>
    {
        protected override void Seed(MusicContext context)
        {
           new List<Album>
            {
                new Album { Title = "The Best Of Men At Work", Genre = "Rock", Price = 8.99M, Artist = "Men At Work", AlbumArtUrl = "/Content/Images/placeholder.gif" ,qtSold=0 },
                new Album { Title = "A Copland Celebration, Vol. I", Genre = "Classical", Price = 8.99M, Artist = "Aaron Copland & London Symphony Orchestra", AlbumArtUrl = "/Content/Images/placeholder.gif",qtSold=0 },
                new Album { Title = "Worlds", Genre = "Jazz", Price = 8.99M, Artist = "Aaron Goldberg", AlbumArtUrl = "/Content/Images/placeholder.gif",qtSold=0 },
                new Album { Title = "For Those About To Rock We Salute You", Genre = "Rock", Price = 8.99M, Artist = "AC/DC", AlbumArtUrl = "/Content/Images/placeholder.gif",qtSold=0 },
                new Album { Title = "Let There Be Rock", Genre = "Rock", Price = 8.99M, Artist = "AC/DC", AlbumArtUrl = "/Content/Images/placeholder.gif",qtSold=0 },
                new Album { Title = "Balls to the Wall", Genre = "Rock", Price = 8.99M, Artist = "Accept", AlbumArtUrl = "/Content/Images/placeholder.gif",qtSold=0 },
                new Album { Title = "Restless and Wild", Genre = "Rock", Price = 8.99M, Artist = "Accept", AlbumArtUrl = "/Content/Images/placeholder.gif",qtSold=0 },
                new Album { Title = "Górecki: Symphony No. 3", Genre = "Classical", Price = 8.99M, Artist = "Adrian Leaper & Doreen de Feis", AlbumArtUrl = "/Content/Images/placeholder.gif",qtSold=0 },
               }.ForEach(a => context.Albuns.Add(a));

            new List<User>{
            new User { Username = "admin", Email = "ad@smt.com", Password = "admin", Type = "admin" },
            new User { Username = "manager", Email = "mg@smt.com", Password = "manager", Type = "manager" },
        }.ForEach(u => context.Users.Add(u));


            
            context.SaveChanges();
        }
    }
}