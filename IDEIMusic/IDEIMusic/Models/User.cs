﻿namespace IDEIMusic.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        public int UserId { get; set; }

        [Required]
        [Display(Name = "UserName: ")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(150)]
        [Display(Name = "Password: ")]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(150)]
        [Display(Name = "Email Address: ")]
        public string Email { get; set; }

        public string Type { get; set; }

        public string API_KEY { get; set; }
    }
}
