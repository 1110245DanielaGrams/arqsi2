﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class Album
    {
        public int AlbumId { get; set; } 
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string AlbumArtUrl { get; set; }
        public string Genre { get; set; }
        public string Artist { get; set; }
        public int qtSold { get; set; }
    }
}