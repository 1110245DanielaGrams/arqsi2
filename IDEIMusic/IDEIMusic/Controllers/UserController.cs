﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using IDEIMusic.DAL;
using IDEIMusic.Models;
using System.Text;
using System.Reflection;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Security.Permissions;
using System.Web.Services.Description;

namespace IDEIMusic.Controllers
{
    public class UserController : Controller
    {
        private MusicContext db = new MusicContext();

        // GET: User
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: User/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    User user = db.Users.Find(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        // GET: User/Create /////////Register
        public ActionResult Register()
        {
            return View();
        }

        // POST: User/Create /////////Register
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserId,Username,Password,Email")] User user)
        {//, string webServiceAsmxUrl, string serviceName, string methodName
            user.Type = "Store";
           // user.API_KEY = "12345";
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            user.API_KEY = g.ToString();
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();

                //System.Net.WebClient client = new System.Net.WebClient();

                ////connect to WS
                //System.IO.Stream stream = client.OpenRead(webServiceAsmxUrl + "?wsdl");

                //// Now read the WSDL file describing the service.

                //ServiceDescription description = ServiceDescription.Read(stream);


                ////Load DOM
                ////initialize service importer

                //ServiceDescriptionImporter importer = new ServiceDescriptionImporter();

                //importer.ProtocolName = "Soap12"; // Use SOAP 1.2.

                //importer.AddServiceDescription(description, null, null);

                //// Generate a proxy client.

                //importer.Style = ServiceDescriptionImportStyle.Client;

                //// Generate properties to represent primitive values.

                //importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;

                //// Initialize a Code-DOM tree into which we will import the service.

                //CodeNamespace nmspace = new CodeNamespace();

                //CodeCompileUnit unit1 = new CodeCompileUnit();

                //unit1.Namespaces.Add(nmspace);

                //// Import the service into the Code-DOM tree. This creates proxy code that uses the service.

                //ServiceDescriptionImportWarnings warning = importer.Import(nmspace, unit1);

                //if (warning == 0) // If zero then we are good to go
                //{

                //    // Generate the proxy code

                //    CodeDomProvider provider1 = CodeDomProvider.CreateProvider("CSharp");

                //    // Compile the assembly proxy with the appropriate references

                //    string[] assemblyReferences = new string[5] { "System.dll", "System.Web.Services.dll", "System.Web.dll", "System.Xml.dll", "System.Data.dll" };

                //    CompilerParameters parms = new CompilerParameters(assemblyReferences);

                //    CompilerResults results = provider1.CompileAssemblyFromDom(parms, unit1);

                //    // Check For Errors

                //    if (results.Errors.Count > 0)
                //    {

                //        foreach (CompilerError oops in results.Errors)
                //        {

                //            System.Diagnostics.Debug.WriteLine("========Compiler error============");

                //            System.Diagnostics.Debug.WriteLine(oops.ErrorText);

                //        }

                //        throw new System.Exception("Compile Error Occured calling webservice. Check Debug ouput window.");

                //    }

                //    // Finally, Invoke the web service method

                //    object wsvcClass = results.CompiledAssembly.CreateInstance(serviceName);

                //    MethodInfo mi = wsvcClass.GetType().GetMethod(methodName);


                //    mi.Invoke(wsvcClass, args);
                //}

                //http://localhost/trabArq2/shopCart/service.php
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
            //return View(user);
        }


        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(Models.User userr)
        {
            var user = db.Users.Where(e => e.Email.ToString() == userr.Email);
            User u = user.First<User>();
            if (userr.Password.Equals(u.Password))
            //            if (ModelState.IsValid)
            {
                Session["User"] = u.Type;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Login details are wrong.");
            }
            return View(userr);
        }


        public ActionResult LogOut()
        {
            Session["User"] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }




        // GET: User/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    User user = db.Users.Find(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        //// POST: User/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "UserId,Username,Password,Email,Type,API_KEY")] User user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(user).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(user);
        //}


        // GET: User/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    User user = db.Users.Find(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        //// POST: User/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    User user = db.Users.Find(id);
        //    db.Users.Remove(user);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
