<?php
// Pull in the NuSOAP code
require_once('nusoap.php');
// Create the server instance
$server = new soap_server();
// Initialize WSDL support
$server->configureWSDL('hellowsdl', 'urn:hellowsdl');
$in=array('x'=>'xsd:int',
          'y'=>'xsd:int');
// Register the method to expose

$server->register('add',                // method name
    $in,        // input parameters
    array('return' => 'xsd:int'),      // output parameters
    'urn:addwsdl',                      // namespace
    'urn:addwsdl#add',                // soapaction
    'rpc',                                // style
    'encoded',                            // use
    'Says hello to the caller'            // documentation
);
// Define the method as a PHP function
function add($x,$y) {
        return $x+$y;
}
// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
