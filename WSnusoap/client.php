<?php

require_once( "nusoap.php" );

$ns="urn:servicename";
$client = new soapclient('http://localhost:85/WSnusoap/server.php?wsdl','wsdl');
if ( $client->getError() ) {
print "<h2>Soap Constructor Error:</h2><pre>".
$client->getError()."</pre>";
}
$params=array("Name"=>"mleiv", "Age"=>35);
$result = $client->call( "DoSomething", array("parameters"=>$params), $ns);
if ($client->fault) { //soap_fault
print "<h2>Soap Fault: </h2><pre>(". $client->fault->faultcode .")  ".
$client->fault->faultstring. "</pre>";
}
elseif ( $client->getError() ) {
print "<h2>Soap Error: </h2><pre>". $client->getError() ."</pre>";
}
else {
print "<h2>Result: </h2><pre>". $result["Pass"] ."</pre>";
}
print '<h2>Details:</h2><hr />'.
'<h3>Request</h3><pre>' .
htmlspecialchars( $client->request, ENT_QUOTES) .'</pre>'.
'<h3>Response</h3><pre>' .
htmlspecialchars( $client->response, ENT_QUOTES) .'</pre>';

?>
